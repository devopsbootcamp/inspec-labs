FROM apline

ADD ./.env /app

WORKDIR /app
ENTRYPOINT ["start.sh"]